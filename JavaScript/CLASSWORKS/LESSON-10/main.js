// Create tag div

const div = document.createElement("div");

// Add class  wraper

div.classList.add("wraperr")

const body = document.querySelector("body")
body.appendChild(div)
console.log(body);

//  Add header

const header = document.createElement("h1")
header.textContent = "Hello world, Today we are working with the DOM"
div.insertAdjacentElement('beforebegin', header)

//  Add list
// const ul = document.createElement("ul")
const ul = `
<ul>
    <li>First</li>
    <li>Second</li>
    <li>Third</li>
</ul>
`;
div.innerHTML = ul

// Add imESge

const img = document.createElement("img");
img.src =
    "https://www.istockphoto.com/resources/images/PhotoFTLP/998044806.jpg"
img.width = 240;
img.classList.add("regular-image");

img.alt = "We are used new featuref to the js app";

div.appendChild(img);

const elemHTML = `
<div class="pDiv">
<p> Paragraph 1 </p>
<p> Paragraph 2 </p>
<p> Paragraph 3 </p>
<p> Paragraph 4 </p>
<p> Paragraph 4 </p>
<p> Paragraph 6 </p>
</div>
`;

const ulList = div.querySelector('ul');
ulList.insertAdjacentHTML("beforebegin", elemHTML);

const pDiv = document.querySelector('.pDiv');
pDiv.children[1].classList.add("text");
console.log(pDiv.children);

// pDiv.firstElementChild.remove()

const generateAuto = (brand, color, year) => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    return `
    <div class="autoCard">
    <h2> ${brand.toUpperCase()} ${year}</h2>
    <p> Авто ${brand.toUpperCase()} ${year}року. Вік авто - ${
        currentYear - year} </p>
    <p> Color: ${color} </p>
    <button type = "button" class = "deleteBtn">DELETE</button>
    </div>
    `;
};

const carsDiv = document.createElement("div");
carsDiv.classList.add("autos");

const carsList = [
    { brand: "Tesla", year: 2015, color: "red"},
    { brand: "Lexus", year: 2016, color: "green"},
    { brand: "Nisan", year: 2012, color: "blue"},
    ];
    
    const carsHTML = carsList
    .map((car)=> {
        return generateAuto(car.brand, car.color, car.year);
    })
    .join("");

carsDiv.innerHTML = carsHTML;
console.log(carsDiv);
// console.log(carsHTML);

div.insertAdjacentElement('beforebegin', carsDiv)



const buttons = document.querySelectorAll(".deleteBtn")
console.log(buttons);

function buttonDelete(e) {
  const currentButton = e.currentTarget;
  currentButton.parentElement.remove();
  console.log(currentButton.parentElement);
}

buttons.forEach(button => {
    button.addEventListener("click", buttonDelete);
});