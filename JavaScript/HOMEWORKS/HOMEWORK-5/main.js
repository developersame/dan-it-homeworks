// Теоретичні питання

// 1. Опишіть своїми словами, що таке метод об'єкту

//  Метод об'єкту - це функції, що знаходяться у властивостях об'єкта.


// 2. Який тип даних може мати значення властивості об'єкта?

//    Може мати будь-якій тип данних.


// 3. Об'єкт це посилальний тип даних. Що означає це поняття?

//    Посилальний тип даних зберігає у собі посилання на об'єкт, а не значення.



// ## Завдання


function createNewUser()
{
  let newUser = new Object();
  newUser.firstName = prompt("Enter you first name");
  newUser.lastName = prompt("Enter you last name");
  newUser.getLogin = function () {
  return `${(newUser.firstName.charAt(0) + newUser.lastName).toLowerCase()}`;
};
  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());

