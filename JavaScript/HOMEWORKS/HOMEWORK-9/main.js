// Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.

//    За допомогою методу - document.createElement(tag)  
//    Наприклад створення тегу div :
//   let div = document.createElement('div');


// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML 
//   і опишіть можливі варіанти цього параметра.
  
//   tagElement.insertAdjacentHTML(position, text);
//   Параметр  position - визначає позицію елемента що додається, 
//                  щодо елемента, що викликав метод.

//   "beforebegin": до відкриваючого тега.
//   "afterbegin": після відкриваючого тега.
//   "beforeend": перед закриваючим тегом .
//   "afterend": після закриваючого тега.



// 3. Як можна видалити елемент зі сторінки?

//     За допомогою методу remove()
//     Наприклад:
//     div.remove();  



// Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

function showElementsList(arr, parentUl = document.body) {

  let ul = document.createElement('ul');

  arr.forEach((elem) => {
    let li = document.createElement('li');
    li.append(elem);
    ul.append(li);
  });
  parentUl.prepend(ul);
}

showElementsList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"] );
